import tweepy
from tweepy import OAuthHandler
import os
import overallconfig as cfg
from datetime import datetime
from nltk.corpus import wordnet as wn
import json
import math

twitter_user_id = {}

auth = OAuthHandler(cfg.twitter_api['consumer_key'], cfg.twitter_api['consumer_secret'])
auth.set_access_token(cfg.twitter_api['access_token'], cfg.twitter_api['access_secret'])

proxy = cfg.proxy_creds['proxyIP'] + ":" + cfg.proxy_creds['proxyPort']

api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True, proxy=proxy)

def create_needed_file_paths():
    print("Setting up different different required data paths")
    for path in cfg.file_paths:
        os_path = os.path.join(*cfg.file_paths[path])
        print("checking existence of : " + os_path)
        if not os.path.exists(os_path):
            print(os_path + " does not exist --> creating it")
            os.makedirs(os_path)
        else:
            print(os_path + "already exists")


def setup():
    # everything called here is needed to setup everything...
    print("starting setup")
    create_needed_file_paths()
    print("ended setup")

def get_twitter_username_by_id(id):
    if not id in twitter_user_id:
        user = api.get_user(id)
        twitter_user_id[id] = user.screen_name
    return twitter_user_id[id]


'''Method necessary to remove that not to JSON serializable error....'''
def converter_for_non_serializable_objetcts(object):
    if isinstance(object, datetime):
        return object.__str__()
    raise ValueError


'''returns a weight for the specified word according to the equitation in the algorithm
:returns None if no weight can't be calculated
'''
def get_emotion_expression_weight(word):
    distance_to_pos = calculate_min_distance_between_words(word, cfg.reference_words['positive'])
    distance_to_neg = calculate_min_distance_between_words(word, cfg.reference_words['negative'])
    distance = None
    if not distance_to_pos and not distance_to_neg:
        return 0 # word is seen as neutral
    if distance_to_neg  and not distance_to_pos:
        distance = distance_to_neg
    elif distance_to_pos and not distance_to_neg:
        distance = distance_to_pos
    else:
        distance = min(distance_to_pos, distance_to_neg)
    if distance >= 0 and distance <= 8:
        return 1.2 - (distance - 1) * 0.02
    elif distance > 8 and distance <= 11:
        return 1 - (distance - 1) * 0.1
    print("distance bigger 11 for word: " + word)
    return 0 # for 11 Value is zero... Value can't be negative...


'''calculcates minimum distance between two words
:returns minmum distance or None if no minimum distance is found
'''
def calculate_min_distance_between_words(word1, word2):
    syns_word1 = wn.synsets(word1)
    syns_word2 = wn.synsets(word2)
    distance = [d for d in [x.shortest_path_distance(y) for x in syns_word1 for y in syns_word2 if x != y] if d != None]
    if not distance:
        return None
    return min(distance)


'''reads all JSON entries from a file which has the following Format:
{JSON},{JSON},{JSON},
'''
def read_JSON_file(file):
    '''reads JSON file and returns an array of JSON data'''
    with open(file, encoding='utf-8') as f:
        data = (line.strip() for line in f)  # Erzeugt Generator Objekt
        data_json = "[{0}]".format(','.join(data))

    charAt =  data_json[len(data_json)-2] # if second to last char is a semicolon, remove it (to ensure its a valid JSON String)
    if charAt == ',':
        data_json = data_json[:len(data_json)-2] + ']'
    data = json.loads(data_json)
    return data


def calculate_tf_idf_weighting(term_occurence, words_in_document, total_number_of_documents, documents_containg_term):
    term_frequency = term_occurence / words_in_document
    inverse_document_frequency = math.log(total_number_of_documents/documents_containg_term)
    return term_frequency*inverse_document_frequency
