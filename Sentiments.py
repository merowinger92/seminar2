import overallconfig as cfg
import helpers
import os
import datetime
import numpy as np
import matplotlib.pyplot as plt

def date_key(a):
    a = datetime.datetime.strptime(a, '%d.%m.%Y').date()
    return a


if __name__ == "__main__":
    path = os.path.join(*cfg.file_paths['clustered'])
    path1 = os.path.join(path, "pos_neg_distribution.json")
    data = helpers.read_JSON_file(path1)
    array = data[0]["2018"]
    sorted_dates_2k18 = sorted(data[0]["2018"].keys(), key=date_key)
    sorted_after = sorted(data[0]["after"].keys(), key=date_key)

    values_after_positiv = []
    values_after_negativ = []
    values_positiv_2k18 = []
    values_negativ_2k18 = []
    for idx, date in enumerate(sorted_dates_2k18):
        print(str(idx) + ": 2018")
        positiv = round((data[0]["2018"][date]["positive"] / (data[0]["2018"][date]["positive"] + data[0]["2018"][date]["negative"])) * 100, 2)
        negativ = round((data[0]["2018"][date]["negative"] / (data[0]["2018"][date]["positive"] + data[0]["2018"][date]["negative"])) * 100, 2)
        values_positiv_2k18.append(positiv)
        values_negativ_2k18.append(negativ)

    for idx, date in enumerate(sorted_after):
        print(str(idx) + ": after")
        positiv = round((data[0]["after"][date]["positive"] / (
                    data[0]["after"][date]["positive"] + data[0]["after"][date]["negative"])) * 100, 2)
        negativ = round((data[0]["after"][date]["negative"] / (
                    data[0]["after"][date]["positive"] + data[0]["after"][date]["negative"])) * 100, 2)
        values_after_positiv.append(positiv)
        values_after_negativ.append(negativ)


    width = 0.35

    index = np.arange(len(sorted_dates_2k18))
    plt1 = plt.bar(index, values_positiv_2k18, width)
    plt2 = plt.bar(index, values_negativ_2k18, width, bottom= values_positiv_2k18)
    plt.ylabel("Prozent %")
    plt.title("Sentimente für Mai 2018")
    plt.xticks(index, sorted_dates_2k18, rotation='vertical')
    plt.yticks(np.arange(0, 101, 10))
    plt.legend((plt1[0], plt2[0]), ("Positiv", "Negativ"), loc='center left', bbox_to_anchor=(1, 0.5))
    plt.subplots_adjust(bottom=0.2)


    plt.show()


    index = np.arange(len(sorted_after))
    plt1 = plt.bar(index, values_after_positiv, width)
    plt2 = plt.bar(index, values_after_negativ, width, bottom= values_after_positiv)
    plt.ylabel("Prozent %")
    plt.title("Sentimente nach 18. September 2015")
    plt.xticks(index, sorted_after, rotation='vertical')
    plt.yticks(np.arange(0, 101, 10))
    plt.legend((plt1[0], plt2[0]), ("Positiv", "Negativ"), loc='center left', bbox_to_anchor=(1, 0.5))
    plt.subplots_adjust(bottom=0.2)

    plt.show()

    print("hannes")