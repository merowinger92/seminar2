import helpers
import os
import overallconfig as cfg
import json
from dateutil.parser import parse
from TweetModel import TweetModel
import collections
from queue import Queue
from threading import Thread
import datetime

def apply_weighting(wordlist_path, sentiment_vector_place, word_vector_path):
    calculate_sentiment_express_weight_vector(wordlist_path, wordlist_path)
    process_weighting(word_vector_path, wordlist_path, sentiment_vector_place)
    return None

def init_word_counter(path_to_word_list):
    data = read_word_list(path_to_word_list)
    count_stemms = {}
    count_tokens = {}
    for stemm in data['stemm_words']:
        count_stemms[stemm] = 0
    for token in data['token_words']:
        count_tokens[token] = 0
    return (count_stemms, count_tokens)

def read_word_list(word_list_file_path):
    json_data = helpers.read_JSON_file(word_list_file_path)
    if len(json_data) > 1:
        print("Error with word list file")
        raise ValueError
    return json_data[0]

def save_sentiment_weights(weights_stemms, weights_token, save_path):
    class SentimentWeights(object):
        def __init__(self):
            self.sentiment_weights_stemms = {}
            self.sentiment_weights_tokens = {}

    senti_weights = SentimentWeights()
    senti_weights.sentiment_weights_tokens = weights_token
    senti_weights.sentiment_weights_stemms = weights_stemms
    print("Sentiment Tokens: ", len(weights_token))
    print("Sentiment Stemms: ", len(weights_stemms))
    print("Saving Sentiment Weighting File....")
    with open(save_path, 'a', encoding='utf-8') as f:
        senti_weights_str = json.dumps(senti_weights.__dict__, default=helpers.converter_for_non_serializable_objetcts)
        f.write(senti_weights_str + ",")

def calculate_sentiment_express_weight_vector(source_path_wordlist, dest_path_vector):
    print("building Sentiment Vectors....")
    wordlist = os.path.join(source_path_wordlist, "wordlist.json")
    sentiment_vector_path = os.path.join(dest_path_vector, "sentiment_vector.json")
    wordlist_content = read_word_list(wordlist)
    sentiment_weights_stemms = {}
    sentiment_weights_tokens = {}
    print("Stemming Vector")
    for key in wordlist_content['stemm_words']:
        sentiment_weight = helpers.get_emotion_expression_weight(key)
        if sentiment_weight > 0:
            sentiment_weights_stemms[key] = sentiment_weight
    print("Token Vector")
    for key in wordlist_content['token_words']:
        sentiment_weight = helpers.get_emotion_expression_weight(key)
        if sentiment_weight > 0:
            sentiment_weights_tokens[key] = sentiment_weight
    save_sentiment_weights(sentiment_weights_stemms, sentiment_weights_tokens, sentiment_vector_path)
    return sentiment_weights_stemms, sentiment_weights_tokens

def read_tweets_from_file(file):
    '''returns Array of TweetModel'''
    tweets = []
    JSON_array = helpers.read_JSON_file(file)
    for entry in JSON_array:
        tweet = TweetModel()
        tweet.id = entry['id']
        tweet.username =  entry['username']
        tweet.user_id = entry['user_id']
        tweet.creation_date = parse(entry['creation_date'])
        tweet.text = entry['text']
        tweet.tokens = entry['tokens']
        tweet.stemmed_tokens = entry['stemmed_tokens']
        tweet.pos_tagged_stems = entry['pos_tagged_stems']
        tweet.emote_stemms = entry['emote_stemms']
        tweet.emote_tokens = entry['emote_tokens']
        tweet.stemm_vecotr = entry['stemm_vecotr']
        tweet.token_vector = entry['token_vector']
        tweet.pre_category = entry['pre_category']
        tweets.append(tweet)
    return tweets


def calculate_tf_idf_weighting(word_occurencies, words_in_doc, document_occurencies, all_number_of_docs):
    tf_idf_vec = {}
    for key in document_occurencies:
        word_occs = word_occurencies[key]
        doc_occs = document_occurencies[key]
        tf_idf_vec[key] = helpers.calculate_tf_idf_weighting(word_occs, words_in_doc, doc_occs, all_number_of_docs)
    return tf_idf_vec


def calculate_completely_Weighted_vector(word_occurencies, tf_idf_vector, sentiment_vector):
    return_vector = {}
    for key in sentiment_vector:
        return_vector[key] = word_occurencies[key] * tf_idf_vector[key] * sentiment_vector[key]

    return return_vector



def weighting_worker(i, enc_queue, save_path, all_doc_count, document_occurencies_count, sentiment_vector):
    while True:
        print("Thread ", i, ": Waiting for work...")
        file_path = enc_queue.get()
        print("Thread: ", i, "Calculating weights for file: ", str(file_path))
        tweets = read_tweets_from_file(file_path)
        for entry in tweets:
            tweet = entry
            stemms = document_occurencies_count['document_occurency_stemm']
            token = document_occurencies_count['document_occurency_token']
            entry.tf_idf_stemm_vector = calculate_tf_idf_weighting(entry.stemm_vecotr, len(entry.tokens), stemms, all_doc_count) # hier richtige Argumente einfügen
            entry.tf_idf_token_vector = calculate_tf_idf_weighting(entry.token_vector, len(entry.tokens), token, all_doc_count) # hier richtige Argumente einfügen

            entry.weighted_vector_stemms = calculate_completely_Weighted_vector(entry.stemm_vecotr, entry.tf_idf_stemm_vector, sentiment_vector['sentiment_weights_stemms'])
            entry.weighted_vector_tokens = calculate_completely_Weighted_vector(entry.token_vector, entry.tf_idf_token_vector, sentiment_vector['sentiment_weights_tokens'])
            save_weighted_data(entry, save_path)
        print("Thread ", i, ": File: ", str(file_path), " done....")
        enc_queue.task_done()


def process_weighting(word_vector_path, word_list_path, final_save_path):
    enclosure_queue = Queue()

    word_list = read_word_list(os.path.join(word_list_path, "wordlist.json"))
    all_tweet_count = word_list['all_tweet_count']

    document_occurencies_json = read_word_list(os.path.join(word_list_path, "document_vector.json"))

    sentiment_vector_json = read_word_list(os.path.join(word_list_path, "sentiment_vector.json"))

    for i in range(10):
        worker = Thread(target=weighting_worker, args=(i, enclosure_queue, final_save_path, all_tweet_count, document_occurencies_json, sentiment_vector_json)) # richtige Parameter anpassen
        worker.setDaemon(True)
        worker.start()

    for file in os.listdir(word_vector_path):
        if file.endswith(".json"):
            print("Queuing file: " + file.__str__())
            filepath = os.path.join(word_vector_path, file)
            enclosure_queue.put(filepath)

    print("waiting for weighting to finish...")
    enclosure_queue.join()

def save_weighted_data(data, path):
    '''strip down data to the most neccessary stuff....'''
    class TweetLite(object):
        def __init__(self):
            self.id = -1
            self.username = ""
            self.user_id = -1
            self.creation_date = datetime.datetime.now()
            self.text = ""
            self.tokens = []
            self.weighted_vector_tokens = {}
            self.weighted_vector_stemms = {}


        def from_usual_tweet(self, usual_tweet):
            self.id = usual_tweet.id
            self.user_id = usual_tweet.user_id
            self.username = usual_tweet.username
            self.creation_date = usual_tweet.creation_date
            self.text = usual_tweet.text
            self.tokens = usual_tweet.tokens
            self.weighted_vector_tokens = usual_tweet.weighted_vector_tokens
            self.weighted_vector_stemms = usual_tweet.weighted_vector_stemms

    temp_path = path
    day = str(data.creation_date.day)
    if len(day) < 2:
        day = '0' + day
    month = str(data.creation_date.month)
    if len(month) < 2:
        month = '0' + month
    file_name = day + month + str(data.creation_date.year) + '.json'
    path = os.path.join(temp_path, file_name)
    with open(path, 'a', encoding='utf-8') as f:
        tweet_str = json.dumps(data.__dict__, default=helpers.converter_for_non_serializable_objetcts)
        f.write(tweet_str + ",")



if __name__ == "__main__":
    print("Start Weighting....")
    wordlist_path = os.path.join(*cfg.file_paths['preprocessed_words'])
    sentiment_vector_path = os.path.join(*cfg.file_paths['preprocessed_sentiment_vectors'])
    word_vectors = os.path.join(*cfg.file_paths['preselected'])
    apply_weighting(wordlist_path, sentiment_vector_path, word_vectors)