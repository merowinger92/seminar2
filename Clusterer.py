import overallconfig as cfg
import os
import helpers
from TweetModel import TweetModel
import json
from dateutil.parser import parse
from sklearn import preprocessing
from sklearn.cluster import KMeans
import pandas
from collections import Counter

def do_clustering(initial_path, end_path, k_means_running_time):
    # first read in tweets
    print("reading in all files...")
    all_file_paths = os.path.join(initial_path, "all_in_one.json")
    all_twt = helpers.read_JSON_file(all_file_paths)
    # init cluster results as emtpy...
    all_inited = init_cluster_runs_for_tweets(all_twt)
    #read in dataframe
    print("Read in Dataframe...")
    df_path = os.path.join(initial_path, "stemm_df.csv")
    df = pandas.read_csv(df_path, index_col=0)

    #record positive and negative indexes
    print("Record positive and negative indexes in dataset...")
    pos_neg_indexes = record_pos_neg_indexes(all_inited)

    # preparation for k-means
    print("normalizing values...")
    df_as_array = df.values
    array_normalized = preprocessing.normalize(df_as_array)

    #init clustering...
    runs = 0
    runs_inc_invalid_runs = 0
    while runs <= k_means_running_time:
        print("Clustering Run: ", runs_inc_invalid_runs)
        print("thereof valid runs: ", runs)
        kmeans = KMeans(n_clusters=2).fit(array_normalized)
        clusters_positives = []
        clusters_negative = []
        for index in pos_neg_indexes['positive']:
            clusters_positives.append(kmeans.labels_[index])
        for index in pos_neg_indexes['negative']:
            clusters_negative.append(kmeans.labels_[index])

        #decide which cluster is postive / negative according to Liu 2012 page 132
        pos_count = Counter(clusters_positives)
        neg_count = Counter(clusters_negative)
        if (pos_count[0] + neg_count[1]) >= (pos_count[1] + neg_count[0]):
            print("valid run")
            runs = runs + 1
            runs_inc_invalid_runs = runs_inc_invalid_runs + 1
            # cluster 0 = positive
            # cluster 1 = negative
            for idx, tweet in enumerate(all_inited):
                pos_neg_cluster = assign_label_to_single_tweet(kmeans.labels_[idx], "positive", "negative")
                tweet['cluster_results'].append(pos_neg_cluster)
        elif (pos_count[0] + neg_count[1]) < (pos_count[1] + neg_count[0]):
            print("valid run")
            runs = runs + 1
            runs_inc_invalid_runs = runs_inc_invalid_runs + 1
            # cluster 1 = positive
            # cluster 0 = negative
            for idx, tweet in enumerate(all_inited):
                pos_neg_cluster = assign_label_to_single_tweet(kmeans.labels_[idx], "negative", "positive")
                tweet['cluster_results'].append(pos_neg_cluster)
        else:
            print("Invalid Run")
            runs_inc_invalid_runs = runs_inc_invalid_runs + 1
            continue


    print("saving all results...")
    save_clustered_data(all_inited, end_path)

    return None

def init_cluster_runs_for_tweets(all_tweets):
    print("Init Clustering Results as empty")
    for twt in all_tweets:
        twt['cluster_results'] = [5,3,1]
    return all_tweets

def record_pos_neg_indexes(all_tweets):
    positive = []
    negative = []
    for idx, tweet in enumerate(all_tweets):
        if tweet['pre_category'] > -1:
            if tweet['pre_category'] == 1:
                positive.append(idx)
            if tweet['pre_category'] == 0:
                negative.append(idx)
    print("We got ", len(positive), " positive Tweets")
    print("We got ", len(negative), " negative Tweets")
    returner = {"positive": positive, "negative": negative}
    return returner

def assign_label_to_single_tweet(label, cluster0, cluster1):
    if label == 0:
        return cluster0
    if label == 1:
        return  cluster1


def save_clustered_data(data, final_path):
    file_name = os.path.join(final_path, "result.json")
    for tda in data:
        with open(file_name, 'a', encoding='utf-8') as f:
            tweet_str = json.dumps(tda, default=helpers.converter_for_non_serializable_objetcts)
            f.write(tweet_str + ",")

if __name__ == "__main__":
    print("starting clusterer...")
    helpers.setup()
    all_diretory = os.path.join(*cfg.file_paths['clustering'])
    final_directory = os.path.join(*cfg.file_paths['clustering'])
    running_times = cfg.clustering_times
    do_clustering(all_diretory, all_diretory, running_times)