import overallconfig as cfg
import helpers
from collections import Counter
import os
import json
from dateutil.parser import parse
from datetime import datetime






if __name__ == "__main__":
    clustering = os.path.join(*cfg.file_paths['preprocessed_dirty'])

    counter = []
    for i in range(0, 280):
        counter.append(0)

    all_data_count = 0
    for file in os.listdir(clustering):
        # print("reading Files: " + file)
        if file.endswith(".json"):
            file_path = os.path.join(clustering, file)
            data = helpers.read_JSON_file(file_path)
            all_data_count = all_data_count + len(data)
            for twt in data:
                counter[len(twt['emote_stemms'])] = counter[len(twt['emote_stemms'])] + 1



    print("Anzahl Datensätze:" + str(all_data_count))
    for idx, count in enumerate(counter):
        print(str(idx) + ": " + str(count))
