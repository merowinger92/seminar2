import overallconfig as cfg
import helpers
from collections import Counter
import os
import json
from dateutil.parser import parse
from datetime import datetime

def do_voting(source_path, final_path):
    day_release = 18
    month_release = 9
    year_release = 2015

    date_release = datetime(year_release, month_release, day_release)
    print("reading huge data file....")
    file = os.path.join(source_path, "result.json")
    data = helpers.read_JSON_file(file)
    print("Voting started...")
    pos_neg_sent_dic = {"2018": {},
                        "before": {},
                        "after": {}}
    for ds in data:
        counter = Counter({})
        counter.update(ds['cluster_results'])
        if counter['negative'] >= counter['positive']:
            ds['overall_sentiment'] = 0 # negativ
        else: # counter[negative] < counter[positive]
            ds['overall_sentiment'] = 1 # positiv

        # record for every date positive /negative sentiments...
        curr_date = parse(ds['creation_date'])
        day = str(curr_date.day)
        if len(day) < 2:
            day = '0' + day
        month = str(curr_date.month)
        if len(month) < 2:
            month = '0' + month
        date_string = day + "." + month + "." + str(curr_date.year)
        if curr_date.year == 2018:
            try:
                if ds['overall_sentiment'] == 1:
                    pos_neg_sent_dic["2018"][date_string]["positive"] = pos_neg_sent_dic["2018"][date_string]["positive"] + 1
                else:
                    pos_neg_sent_dic["2018"][date_string]["negative"] = pos_neg_sent_dic["2018"][date_string]["negative"] + 1
            except KeyError:
                pos_neg_sent_dic["2018"][date_string] = {"positive": 0, "negative": 0}
                if ds['overall_sentiment'] == 1:
                    pos_neg_sent_dic["2018"][date_string]["positive"] = 1 # initializing
                else:
                    pos_neg_sent_dic["2018"][date_string]["negative"] = 1 # initializing
            continue
        time_delta = date_release - curr_date
        if time_delta.days > 0: # before files...
            try:
                if ds['overall_sentiment'] == 1:
                    pos_neg_sent_dic["before"][date_string]["positive"] = pos_neg_sent_dic["before"][date_string]["positive"] + 1
                else:
                    pos_neg_sent_dic["before"][date_string]["negative"] = pos_neg_sent_dic["before"][date_string]["negative"] + 1
            except KeyError:
                pos_neg_sent_dic["before"][date_string] = {"positive": 0, "negative": 0}
                if ds['overall_sentiment'] == 1:
                    pos_neg_sent_dic["before"][date_string]["positive"] = 1 # initializing
                else:
                    pos_neg_sent_dic["before"][date_string]["negative"] = 1 # initializing
            continue
        # after files...
        try:
            if ds['overall_sentiment'] == 1:
                pos_neg_sent_dic["after"][date_string]["positive"] = pos_neg_sent_dic["after"][date_string][
                                                                        "positive"] + 1
            else:
                pos_neg_sent_dic["after"][date_string]["negative"] = pos_neg_sent_dic["after"][date_string][
                                                                        "negative"] + 1
        except KeyError:
            pos_neg_sent_dic["after"][date_string] = {"positive": 0, "negative": 0}
            if ds['overall_sentiment'] == 1:
                pos_neg_sent_dic["after"][date_string]["positive"] = 1  # initializing
            else:
                pos_neg_sent_dic["after"][date_string]["negative"] = 1  # initializing
        continue

    # save everything...
    print("saving all results...")
    save_clustered_data(data, final_path)

    # save also pos/neg distributions...
    pos_neg_file = os.path.join(final_path, "pos_neg_distribution.json")
    with open(pos_neg_file, 'a', encoding='utf-8') as f:
        tweet_str = json.dumps(pos_neg_sent_dic, default=helpers.converter_for_non_serializable_objetcts)
        f.write(tweet_str + ",")

def save_clustered_data(data, final_path):
    file_name = os.path.join(final_path, "result.json")
    for tda in data:
        with open(file_name, 'a', encoding='utf-8') as f:
            tweet_str = json.dumps(tda, default=helpers.converter_for_non_serializable_objetcts)
            f.write(tweet_str + ",")



if __name__ == "__main__":
   # clustering = os.path.join(*cfg.file_paths['clustering'])
   # final = os.path.join(*cfg.file_paths['clustered'])
   # do_voting(clustering, final)
   a = "21.05.2018"
   a = datetime.strptime(a, '%d.%m.%Y').date()
   print("hannes")