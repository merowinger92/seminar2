import AlgorithmController as algo
import overallconfig as cfg
import helpers
import nltk

args = []

print('The nltk version is {}.'.format(nltk.__version__))
algo.run(cfg.file_paths, args)
print("")