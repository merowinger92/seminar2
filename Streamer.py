import dataProvider
import helpers
import overallconfig as cfg

helpers.setup()
terms = " ".join(cfg.search_query['terms'])
print("Configured Search terms are: " + terms)
dataProvider.stream(cfg.search_query['terms'])