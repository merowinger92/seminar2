import overallconfig as cfg
import os
import helpers
from TweetModel import TweetModel
import json
from dateutil.parser import parse
from sklearn import preprocessing
from sklearn.cluster import KMeans
import pandas
from collections import Counter

def do_clustering(source_path, dest_path, k_means_running_time):
    all_tweets = fetch_all_files_together(source_path, dest_path)
    pos_neg_indexes = {}
    pos_neg_indexes = record_pos_neg_indexes(all_tweets)
    print("Building Dataframes...")
    print("First")
    # stemm_dataframe = pandas.DataFrame(data=all_tweets[0].weighted_vector_stemms)
    stemm_dataframe = pandas.DataFrame(data=all_tweets[0].weighted_vector_stemms, index=[0])
    for idx, tweet in enumerate(all_tweets):
        if idx > 0:
            print(idx)
            temp_dataframe = pandas.DataFrame(data=tweet.weighted_vector_stemms, index=[0])
            stemm_dataframe = pandas.concat([stemm_dataframe, temp_dataframe], ignore_index=True)
    print("Second")
    # token_dataframe = pandas.DataFrame(data=all_tweets[0].weighted_vector_tokens)
    token_dataframe = pandas.DataFrame(data=all_tweets[0].weighted_vector_tokens, index=[0])
    for idx, tweet in enumerate(all_tweets):
        if idx > 0:
            print(idx)
            temp_dataframe = pandas.DataFrame(data=tweet.weighted_vector_tokens, index=[0])
            token_dataframe = pandas.concat([token_dataframe, temp_dataframe], ignore_index=True)
    # save dataframes just to be sure....
    print("saving dataframes")
    stemm_dataframe_path = os.path.join(dest_path, "stemm_df.csv")
    stemm_dataframe.to_csv(stemm_dataframe_path)
    token_dataframe_path = os.path.join(dest_path, "token_df.csv")
    token_dataframe.to_csv(token_dataframe_path)

    stemms_array = stemm_dataframe.values
    token_array = token_dataframe.values

    stemms_norm = preprocessing.normalize(stemms_array)
    token_norm = preprocessing.normalize(token_array)

    all_tweets = init_cluster_runs_for_tweets(all_tweets)

    runs = 0
    runs_inc_invalid_runs = 0
    while runs <= k_means_running_time:
        print("Clustering Run: ", runs_inc_invalid_runs)
        print("thereof valid runs: ", runs)
        kmeans = KMeans(n_clusters=2).fit(stemms_norm)
        clusters_positives = []
        clusters_negative = []
        for index in pos_neg_indexes['positive']:
            clusters_positives.append(kmeans.labels_[index])
        for index in pos_neg_indexes['negative']:
            clusters_negative.append(kmeans.labels_[index])

        #decide which cluster is postive / negative
        pos_count = Counter(clusters_positives)
        neg_count = Counter(clusters_negative)
        if pos_count['0'] > pos_count['1'] and neg_count['0'] < neg_count[1]:
            print("valid run")
            runs = runs + 1
            runs_inc_invalid_runs = runs_inc_invalid_runs + 1
            # cluster 0 = positive
            # cluster 1 = negative
            for idx, tweet in enumerate(all_tweets):
                pos_neg_cluster = assign_label_to_single_tweet(kmeans.labels_[idx], "positive", "negative")
                tweet.cluster_results.append(pos_neg_cluster)
        elif pos_count['0'] < pos_count['1'] and neg_count['0'] > neg_count[1]:
            print("valid run")
            runs = runs + 1
            runs_inc_invalid_runs = runs_inc_invalid_runs + 1
            # cluster 1 = positive
            # cluster 0 = negative
            for idx, tweet in enumerate(all_tweets):
                pos_neg_cluster = assign_label_to_single_tweet(kmeans.labels_[idx], "negative", "positive")
                tweet.cluster_results.append(pos_neg_cluster)
        else:
            print("Invalid Run")
            runs_inc_invalid_runs = runs_inc_invalid_runs + 1
            continue

    ####write clustering Reults to File
    clustered_results = os.path.join(dest_path, "final_clustering")
    print("saving final results....")
    count = 0
    for tweet in all_tweets:
        count = count + 1
        print(count)
        with open(clustered_results, 'a', encoding='utf-8') as f:
            tweet_str = json.dumps(tweet.__dict__, default=helpers.converter_for_non_serializable_objetcts)
            f.write(tweet_str + ",")
    return all_tweets
    return None

def assign_label_to_single_tweet(label, cluster0, cluster1):
    if label == 0:
        return cluster0
    if label == 1:
        return  cluster1

def record_pos_neg_indexes(all_tweets):
    positive = []
    negative = []
    for idx, tweet in enumerate(all_tweets):
        print("processing....")
        if tweet.pre_category > -1:
            if tweet.pre_category == 0:
                negative.append(idx)
                continue
            if tweet.pre_category == 1:
                positive.append(idx)
                continue
    print("We got ", len(positive), " positive Tweets")
    print("We got ", len(negative), " negative Tweets")
    returner = {"positive": positive, "negative": negative}
    return returner

def init_cluster_runs_for_tweets(all_tweets):
    print("Init Cluster Results as empty")
    for twt in all_tweets:
        twt.cluster_results = []
    return all_tweets

def read_tweets_from_file(file):
    '''returns Array of TweetModel'''
    tweets = []
    JSON_array = helpers.read_JSON_file(file)
    for entry in JSON_array:
        tweet = TweetModel()
        tweet.id = entry['id']
        tweet.username =  entry['username']
        tweet.user_id = entry['user_id']
        tweet.creation_date = parse(entry['creation_date'])
        tweet.text = entry['text']
        tweet.tokens = entry['tokens']
        tweet.weighted_vector_stemms = entry['weighted_vector_stemms']
        tweet.weighted_vector_tokens = entry['weighted_vector_tokens']
        tweet.pre_category = entry['pre_category']
        tweets.append(tweet)
    return tweets

def read_all_tweets_from_dir(directory):
    all_tweets = []
    for file in os.listdir(directory):
        if file.endswith(".json"):
            print("adding file: ", file, "....")
            temp_path = os.path.join(directory, file)
            single_files_tweets = read_tweets_from_file(temp_path)
            all_tweets.extend(single_files_tweets)
    return all_tweets

def fetch_all_files_together(source_path, dest_path):
    all_tweets = read_all_tweets_from_dir(source_path)
    '''saving them to one file...'''
    path = os.path.join(dest_path, "all_in_one.json")
    for tweet in all_tweets:
        with open(path, 'a', encoding='utf-8') as f:
            tweet_str = json.dumps(tweet.__dict__, default=helpers.converter_for_non_serializable_objetcts)
            f.write(tweet_str + ",")
    return all_tweets

if __name__ == "__main__":
    print("starting clustering...")
    helpers.setup()
    source_path_weighted = os.path.join(*cfg.file_paths['preprocessed_sentiment_vectors'])
    dest_path_clustering = os.path.join(*cfg.file_paths['clustering'])
    running_times = cfg.clustering_times
    do_clustering(source_path_weighted, dest_path_clustering, running_times)