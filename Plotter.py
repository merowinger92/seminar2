import numpy as np
import matplotlib.pyplot as plt
import os
import overallconfig as cfg
from dateutil.parser import parse
import helpers
from datetime import datetime

day_release = 18
month_release = 9
year_release = 2015

date_release = datetime(year_release, month_release, day_release)

all_count_before = 0
file_count_before = 0
all_count_after = 0
file_count_after = 0
all_count_2k18 = 0
file_count_2k18 = 0

dates_unsorted_2k18 = []
dates_unsorted_before = []
dates_unsorted_after = []
data_2k18 = []
data_before = []
data_After = []


bar_width = 0.35


raw_path = os.path.join(*cfg.file_paths['rawdata'])


for file in os.listdir(raw_path):
    if file.endswith(".json"): # it's a json file
        file_path = os.path.join(raw_path, file)
        data = helpers.read_JSON_file(file_path)
        curr_date = parse(data[0]['creation_date'])
        if file.endswith("2018.json"): # it's a 2018 file
            file_count_2k18 = file_count_2k18 + 1
            all_count_2k18 = all_count_2k18 + len(data)
            dates_unsorted_2k18.append(curr_date)
            continue
        delta_time = date_release - curr_date
        # 2015 files...
        if delta_time.days > 0: # before files
            file_count_before = file_count_before + 1
            all_count_before = all_count_before + len(data)
            dates_unsorted_before.append(curr_date)
            continue
        # 2015 files after...
        file_count_after = file_count_after + 1
        all_count_after = all_count_after + len(data)
        dates_unsorted_after.append(curr_date)


dates_unsorted_2k18.sort()
dates_unsorted_after.sort()
dates_unsorted_before.sort()
print("2018:")
ges_percentage = 0
for idx, date in enumerate(dates_unsorted_2k18):
    dates_unsorted_2k18[idx] = date.day.__str__() + "." + date.month.__str__() + "." + date.year.__str__()
    day_str = date.day.__str__()
    mont_str = date.month.__str__()
    if len(day_str) < 2:
        day_str = '0' + day_str
    if len(mont_str) < 2:
        mont_str = '0' + mont_str
    file_str =  day_str + mont_str + date.year.__str__() +".json"
    file_path = os.path.join(raw_path, file_str)
    json_data = helpers.read_JSON_file(file_path)
    percentage = round((len(json_data) / all_count_2k18) * 100, 2)
    data_2k18.append(percentage)
    ges_percentage = ges_percentage + percentage
ges_percentage = 0
for idx, date in enumerate(dates_unsorted_after):
    dates_unsorted_after[idx] = date.day.__str__() + "." + date.month.__str__() + "." + date.year.__str__()
    day_str = date.day.__str__()
    mont_str = date.month.__str__()
    if len(day_str) < 2:
        day_str = '0' + day_str
    if len(mont_str) < 2:
        mont_str = '0' + mont_str
    file_str =  day_str + mont_str + date.year.__str__() +".json"
    file_path = os.path.join(raw_path, file_str)
    json_data = helpers.read_JSON_file(file_path)
    percentage = round((len(json_data) / all_count_after)* 100, 2)
    data_After.append(percentage)
    ges_percentage = ges_percentage + percentage
print("before")
ges_percentage = 0
for idx, date in enumerate(dates_unsorted_before):
    dates_unsorted_before[idx] = date.day.__str__() + "." + date.month.__str__() + "." + date.year.__str__()
    day_str = date.day.__str__()
    mont_str = date.month.__str__()
    if len(day_str) < 2:
        day_str = '0' + day_str
    if len(mont_str) < 2:
        mont_str = '0' + mont_str
    file_str =  day_str + mont_str + date.year.__str__() +".json"
    file_path = os.path.join(raw_path, file_str)
    json_data = helpers.read_JSON_file(file_path)
    percentage = round((len(json_data) / all_count_before)* 100, 2)
    data_before.append(percentage)
    ges_percentage = ges_percentage + percentage
print("")

index = np.arange(file_count_before)


plot1 = plt.bar(index, data_before)
plt.ylabel('Prozent %')
plt.title('Verteilung der Tweets auf Tage in Prozent %')
plt.xticks(index, dates_unsorted_before, rotation='vertical')
plt.yticks(np.arange(0, 31, 5))
plt.subplots_adjust(bottom=0.2)
plt.show()

index = np.arange(file_count_after)
plot2 = plt.bar(index, data_After)
plt.ylabel('Prozent %')
plt.title('Verteilung der Tweets auf Tage in Prozent %')
plt.xticks(index, dates_unsorted_after, rotation='vertical')
plt.subplots_adjust(bottom=0.2, right=0.95, top=0.95, left=0.06)
plt.show()



index = np.arange(file_count_2k18)
plot3 = plt.bar(index, data_2k18)
plt.ylabel('Prozent %')
plt.title('Verteilung der Tweets auf Tage in Prozent %')
plt.xticks(index, dates_unsorted_2k18, rotation='vertical')
plt.subplots_adjust(bottom=0.2)
plt.show()
print("")

