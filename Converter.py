import helpers
import os
from pathlib import Path
from TweetModel import TweetModel
from dateutil.parser import parse
import json
import collections
import overallconfig as cfg


def convert_tweets_to_vectors(source_path, dest_path_word_vetors, dest_path):
    create_word_list(source_path, dest_path)
    generate_word_vectors(source_path, dest_path, dest_path_word_vetors)
    return None



def create_word_list(source_path, dest_path):
    '''1. check if already a wordlist.json exists and continue this list
       2. if not start new list
       3. read in source_path directory
            4. for every file check if tokens/stemms are already in word list, if not add them
    '''
    print("Building WordList...")
    wordlist_file_path = os.path.join(dest_path, "wordlist.json")
    wordlist_file = Path(wordlist_file_path)
    word_list_data_stemms = []
    word_list_data_tokens = []
    stemms_removed = []
    token_removed = []
    overall_tweet_count = 0
    if wordlist_file.exists():
        print("Found existing word list...")
        word_list = read_word_list(wordlist_file_path)
        word_list_data_stemms = word_list['stemm_words']
        word_list_data_tokens = word_list['token_words']

    for file in os.listdir(source_path):
        if file.endswith(".json"):
            file_path = os.path.join(source_path, file)
            tweets = read_tweets_from_file(file_path)
            overall_tweet_count = overall_tweet_count + len(tweets) + 1
            for entry in tweets:
                for stemm in entry.emote_stemms:
                    if stemm in stemms_removed:
                        print("stemm already removed: ", stemm)
                        continue
                    if stemm not in word_list_data_stemms:
                        weight = helpers.get_emotion_expression_weight(stemm)
                        if weight > 0:
                            word_list_data_stemms.append(stemm)
                        else:
                            stemms_removed.append(stemm)
                for token in entry.emote_tokens:
                    if token in token_removed:
                        print("token already removed: ", token)
                        continue
                    if token not in word_list_data_tokens:
                        weight = helpers.get_emotion_expression_weight(token)
                        if weight > 0:
                            word_list_data_tokens.append(token)
                        else:
                            token_removed.append(token)

    '''save the wordlist in wordlist.json'''
    print("Wordlist for stemmed words contains ", len(word_list_data_stemms), " entries....")
    print("Wordlist for token words contains ", len(word_list_data_tokens), " entries....")
    save_wordlists_in_file(wordlist_file_path, word_list_data_stemms, word_list_data_tokens, overall_tweet_count)



def read_tweets_from_file(file_name):
    tweets = []
    JSON_entries = helpers.read_JSON_file(file_name)
    for entry in JSON_entries:
        tweet = TweetModel()
        tweet.id = entry['id']
        tweet.username =  entry['username']
        tweet.user_id = entry['user_id']
        tweet.creation_date = parse(entry['creation_date'])
        tweet.text = entry['text']
        tweet.tokens = entry['tokens']
        tweet.stemmed_tokens = entry['stemmed_tokens']
        tweet.pos_tagged_stems = entry['pos_tagged_stems']
        tweet.pos_tagged_tokens = entry['pos_tagged_tokens']
        tweet.emote_stemms = entry['emote_stemms']
        tweet.emote_tokens = entry['emote_tokens']
        tweets.append(tweet)
    return tweets


def save_wordlists_in_file(path, stemm_words, token_words, sum_tweets):
    class WordList(object):
        def __init__(self):
            self.stemm_words = []
            self.token_words = []
            self.all_tweet_count = 0

    word_list = WordList()
    word_list.stemm_words = stemm_words
    word_list.token_words = token_words
    word_list.all_tweet_count = sum_tweets
    '''save this shit'''
    print("saving wordlists...")
    with open(path, 'a', encoding='utf-8') as f:
        wordlist_str = json.dumps(word_list.__dict__, default=helpers.converter_for_non_serializable_objetcts)
        f.write(wordlist_str + ",")


def read_word_list(word_list_file_path):
    json_data = helpers.read_JSON_file(word_list_file_path)
    if len(json_data) > 1:
        print("Error with word list file")
        raise ValueError
    return json_data[0]


def init_word_counter(path_to_word_list):
    data = read_word_list(path_to_word_list)
    count_stemms = {}
    count_tokens = {}
    for stemm in data['stemm_words']:
        count_stemms[stemm] = 0
    for token in data['token_words']:
        count_tokens[token] = 0
    return (count_stemms, count_tokens)


def generate_word_vectors(path_to_tweets_inc_words, wordlist_dir, word_vector_dir):
    print("Starting to build Word vectors...")
    wordlist_path = os.path.join(wordlist_dir, "wordlist.json")
    overall_stemm_dict = {}
    overall_token_dict = {}
    overall_stemm_dict, overall_token_dict = init_word_counter(wordlist_path)
    for file in os.listdir(path_to_tweets_inc_words):
        if file.endswith(".json"):
            print("current File: ", file)
            whole_file_path = os.path.join(path_to_tweets_inc_words, file)
            all_tweets_in_file = read_tweets_from_file(whole_file_path)
            for tweet in all_tweets_in_file:
                curr_tweet_count_stemm = {}
                curr_tweet_count_token = {}
                curr_tweet_count_stemm, curr_tweet_count_token = init_word_counter(wordlist_path)
                curr_tweet_counter_stemm = collections.Counter(curr_tweet_count_stemm)
                curr_tweet_counter_token = collections.Counter(curr_tweet_count_token)
                entry = TweetModel()
                entry = tweet
                curr_tweet_counter_stemm.update(entry.emote_stemms)
                curr_tweet_counter_token.update(entry.emote_tokens)
                for key in overall_stemm_dict:
                    if curr_tweet_counter_stemm[key] > 0:
                        overall_stemm_dict[key] = overall_stemm_dict[key] + 1
                for key in overall_token_dict:
                    if curr_tweet_counter_token[key] > 0:
                        overall_token_dict[key] = overall_token_dict[key] + 1
                entry.stemm_vecotr = curr_tweet_counter_stemm
                entry.token_vector = curr_tweet_counter_token
                '''save results for every single tweet'''
                word_vector_path = os.path.join(word_vector_dir, file)
                with open(word_vector_path, 'a', encoding='utf-8') as f:
                    tweet_str = json.dumps(entry.__dict__, default=helpers.converter_for_non_serializable_objetcts)
                    f.write(tweet_str + ",")
            print("Finished processing file: ", file)
    print("Finished Calculating all Word Vectors....")
    '''save over all count'''
    document_occurency_path = os.path.join(wordlist_dir, "document_vector.json")
    class DocumentVector(object):
        def __init__(self):
            self.document_occurency_stemm = {}
            self.document_occurency_token = {}

    docVec = DocumentVector()
    print("Writing Down Overall Occurency in words...")
    docVec.document_occurency_stemm = overall_stemm_dict
    docVec.document_occurency_token = overall_token_dict
    with open(document_occurency_path, 'a', encoding='utf-8') as f:
        doc_str = json.dumps(docVec.__dict__, default=helpers.converter_for_non_serializable_objetcts)
        f.write(doc_str + ",")
    print("Finished writing down overall occurency of words...")

if __name__ == "__main__":
    helpers.setup()
    preproc_directory = os.path.join(*cfg.file_paths['preprocessed_dirty'])
    words_path = os.path.join(*cfg.file_paths['preprocessed_words'])
    word_vectors = os.path.join(*cfg.file_paths['preprocessed_counted_words'])
    convert_tweets_to_vectors(preproc_directory, word_vectors, words_path) # Pfade einsetzen