from datetime import datetime

class TweetModel(object):

    def __init__(self):
        self.id = -1
        self.username = ""
        self.user_id = -1
        self.text = ""
        self.creation_date = None