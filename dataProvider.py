import overallconfig as cfg
import tweepy
from tweepy import OAuthHandler
import json
import time
from urllib.parse import quote_plus
from TweepyCustomListener import TweepyCustomListener
from TweetModel import TweetModel
import os
import helpers
import GetOldTweets


''' API access of tweepy'''
auth = OAuthHandler(cfg.twitter_api['consumer_key'], cfg.twitter_api['consumer_secret'])
auth.set_access_token(cfg.twitter_api['access_token'], cfg.twitter_api['access_secret'])

proxy = cfg.proxy_creds['proxyIP'] + ":" + cfg.proxy_creds['proxyPort']

api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True, proxy=proxy)


'''Streaming data of twitter search'''
def stream(hashtag):
    print("Starting Twitter Stream Listener")
    myStreamListener = TweepyCustomListener(api=api)
    myStream = tweepy.Stream(auth=auth, listener=myStreamListener)
    myStream.filter(track=hashtag,languages=["en"])


'''help function for crawling'''
def emptyBuffer(tweets):
    for twt in tweets:
        twiff = TweetModel()
        twiff.id = twt.id
        twiff.username = twt.username
        twiff.user_id = twt.author_id
        twiff.text = twt.text
        twiff.creation_date = twt.date
        day = str(twiff.creation_date.day)
        if len(day) < 2:
            day = '0' + day
        month = str(twiff.creation_date.month)
        if len(month) < 2:
            month = '0' + month
        file_name = day + month + str(twiff.creation_date.year) + '.json'
        temp_path = os.path.join(*cfg.file_paths['rawdata'])
        path = os.path.join(temp_path, file_name)
        with open(path, 'a', encoding='utf-8') as f:
            tweet_str = json.dumps(twiff.__dict__, default=helpers.converter_for_non_serializable_objetcts)
            f.write(tweet_str + ",")


''' startdate and enddate needs to be in the form of YYYY-MM-DD
    results are automatically saved to JSON files name DDMMYYYY.json
    crawls historical data via the twitter website
'''
def crawl(query, startdate, enddate):
    print("starting Crawler for dates " + startdate + " until " +enddate)
    tweetCriteria = GetOldTweets.got.manager.TweetCriteria().setQuerySearch(query).setSince(startdate).setUntil(enddate).setLang("en")
    tweet = GetOldTweets.got.manager.TweetManager.getTweets(tweetCriteria, receiveBuffer=emptyBuffer, bufferLength=cfg.search_buffer['size'], proxy=True)
    return tweet