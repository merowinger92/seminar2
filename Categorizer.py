import helpers
from TweetModel import TweetModel
from dateutil.parser import parse
import overallconfig as cfg
import os
from random import randint
import json
import collections


def read_tweets_from_file_and_init_category(file):
    '''returns Array of TweetModel'''
    tweets = []
    JSON_array = helpers.read_JSON_file(file)
    for entry in JSON_array:
        tweet = TweetModel()
        tweet.id = entry['id']
        tweet.username =  entry['username']
        tweet.user_id = entry['user_id']
        tweet.creation_date = parse(entry['creation_date'])
        tweet.text = entry['text']
        tweet.tokens = entry['tokens']
        tweet.stemmed_tokens = entry['stemmed_tokens']
        tweet.pos_tagged_stems = entry['pos_tagged_stems']
        tweet.emote_stemms = entry['emote_stemms']
        tweet.emote_tokens = entry['emote_tokens']
        tweet.stemm_vecotr = entry['stemm_vecotr']
        tweet.token_vector = entry['token_vector']
        tweet.pre_category = -1
        tweets.append(tweet)
    return tweets

def read_all_tweets_from_dir(directory):
    all_tweets = []
    for file in os.listdir(directory):
        if file.endswith(".json"):
            print("adding file: ", file, "....")
            temp_path = os.path.join(directory, file)
            single_files_tweets = read_tweets_from_file_and_init_category(temp_path)
            all_tweets.extend(single_files_tweets)
    return all_tweets

def save_categorized_data(data, path):
    temp_path = path
    day = str(data.creation_date.day)
    if len(day) < 2:
        day = '0' + day
    month = str(data.creation_date.month)
    if len(month) < 2:
        month = '0' + month
    file_name = day + month + str(data.creation_date.year) + '.json'
    path = os.path.join(temp_path, file_name)
    with open(path, 'a', encoding='utf-8') as f:
        tweet_str = json.dumps(data.__dict__, default=helpers.converter_for_non_serializable_objetcts)
        f.write(tweet_str + ",")

def voting(source_path, target_path):
    tweets_to_be_voted = read_all_tweets_from_dir(source_path)
    print("Total Count: ", len(tweets_to_be_voted))
    last_idx = len(tweets_to_be_voted) - 1
    input_data = 5
    count = 0
    while input_data >= 0:
        idx = randint(0, last_idx)
        if tweets_to_be_voted[idx].pre_category > -1:
            continue
        print("Already categorized: ", count)
        print(idx,": ",tweets_to_be_voted[idx].text)
        try:
            input_data = int(input("Bitte Sentiment eingeben (0 = negativ, 1 = positiv, -1 = Abbruch"))
            if input_data < 0:
                continue
            count = count + 1
            tweets_to_be_voted[idx].pre_category = input_data
        except ValueError:
            pass
        except KeyboardInterrupt:
            pass
        except Exception:
            pass
    print("Total Categorized: ", count)
    #save results...
    for tweet in tweets_to_be_voted:
        save_categorized_data(tweet, target_path)



if __name__ == "__main__":
    #counted_words = os.path.join(*cfg.file_paths['preprocessed_counted_words']) #  source
    #preselected = os.path.join(*cfg.file_paths['preselected']) #  target
    #voting(counted_words, preselected)
    raw_path = os.path.join(*cfg.file_paths['rawdata'])
    for file in os.listdir(raw_path):
        if file.endswith(".json"):
            temmm = os.path.join(raw_path, file)
            tweet = helpers.read_JSON_file(temmm)