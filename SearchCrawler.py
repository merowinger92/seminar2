import overallconfig as cfg
import helpers
import dataProvider
import os
import json
from TweetModel import TweetModel


helpers.setup()
query_search = ""
for terms in cfg.search_query['terms'][:-1]:
    query_search = terms + " " + cfg.search_query['operator'] + " " + query_search
query_search = query_search + cfg.search_query['terms'][-1]
print("starting Crawler with following Criteria: " + query_search)
results = []
for key in cfg.past_search_periods:
    start = cfg.past_search_periods[key]['start']
    end = cfg.past_search_periods[key]['end']
    tweets = dataProvider.crawl(query_search, start, end)
    results.append(tweets)

print("saving all " + str(len(results)) +  " results of crawler....")
# save everything to .json files...
for twt in results:
    twiff = TweetModel()
    twiff.id = twt.id
    twiff.username = twt.username
    twiff.user_id = twt.author_id
    twiff.text = twt.text
    twiff.creation_date = twt.date
    day = str(twiff.creation_date.day)
    if len(day) < 2:
        day = '0' + day
    month = str(twiff.creation_date.month)
    if len(month) < 2:
        month = '0' + month
    file_name = day + month + str(twiff.creation_date.year) + '.json'
    path = os.path.join(*cfg.file_paths['rawdata_crawled'], file_name)
    with open(path, 'a', encoding='utf-8') as f:
        tweet_str = json.dumps(twiff.__dict__, default=helpers.converter_for_non_serializable_objetcts)
        f.write(tweet_str + ",")

