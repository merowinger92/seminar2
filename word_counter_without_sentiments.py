import overallconfig as cfg
import os
import helpers
from TweetModel import TweetModel
import json
from dateutil.parser import parse
from collections import Counter
from nltk.corpus import stopwords
from datetime import datetime
import collections
import matplotlib.pyplot as plt
import numpy as np



preselected = os.path.join(*cfg.file_paths['preselected'])

day_release = 18
month_release = 9
year_release = 2015

date_release = datetime(year_release, month_release, day_release)
# stopwords by wordnet
stop_words = set(stopwords.words('english'))

stop_words_to_remove = [":", ".", "RT", ",", "-", "!", "The", u"\u2019", "I", "?", "(", ")", "#VW", "/", "@Volkswagen",
                        "#Volkswagen", "&", "This", "#volkswagen", "$", "Please", "\\", "via", "like", "#vw", "New", "A",
                        "2018", "We", "S", "'", "#flat4", "get", "U", "#Parts", "US", "With", u"\u00a3", "one", "@VWGroup",
                        "+", "need", "You", "How", "3", "right", "1", "@VW", "...", "\"", "2", "30", u"\u2026", u"\u2013",
                        "#VWGroup", "|", "R", "make", "want", "vw", "It's", "For", "for", u"\U0001f646", "@YouTube",
                        "back", "15", "us", u"\ufe0f", "first", "look", "VW", "Volkswagen", "@ii_naome",
                        "https://t.co/VZS6vj1CU8", "%", "2050", "#environment", "http://onforb.es/1hsxrrD", "#tech",
                        "#cars", "they're", "..", "emission", "Emission", "says", "EPA", "Volkswagen's", "#dieselgate",
                        "#VWGate", "could", "take", "90", "CO2", "@MilWolfMylo", "going", "trying"]


counter_before_dirty = collections.Counter({})
counter_after_dirty = collections.Counter({})
counter_2k18_dirty = collections.Counter({})

overall_pos = 0
overall_neg = 0

for file in os.listdir(preselected):
    if file.endswith(".json"):
        file_path = os.path.join(preselected, file)
        data = helpers.read_JSON_file(file_path)
        if file.endswith("2018.json"):
            for tweet in data:
                print("2018")
                sett = set(tweet['tokens']).difference(stop_words)
                counter_2k18_dirty.update(sett)
                if tweet['pre_category'] > -1:
                    if tweet['pre_category'] == 0:
                        overall_neg = overall_neg + 1
                    if tweet['pre_category'] == 1:
                        overall_pos = overall_pos + 1
            continue
        curr_date = parse(data[0]['creation_date'])
        delta_time = date_release - curr_date
        if delta_time.days > 0:  # before files
            for tweet in data:
                print("before")
                sett = set(tweet['tokens']).difference(stop_words)
                counter_before_dirty.update(sett)
                if tweet['pre_category'] > -1:
                    if tweet['pre_category'] == 0:
                        overall_neg = overall_neg + 1
                    if tweet['pre_category'] == 1:
                        overall_pos = overall_pos + 1
            continue
        #after files...
        for tweet in data:
            print("after")
            sett = set(tweet['tokens']).difference(stop_words)
            counter_after_dirty.update(sett)
            if tweet['pre_category'] > -1:
                if tweet['pre_category'] == 0:
                    overall_neg = overall_neg + 1
                if tweet['pre_category'] == 1:
                    overall_pos = overall_pos + 1






#sort all dictionaries...
print("sort everything")

sorted_2k18 = [(k, counter_2k18_dirty[k]) for k in sorted(counter_2k18_dirty, key=counter_2k18_dirty.get, reverse=True)]
sorted_after = [(k, counter_after_dirty[k]) for k in sorted(counter_after_dirty, key=counter_after_dirty.get, reverse=True)]
sorted_before = [(k, counter_before_dirty[k]) for k in sorted(counter_before_dirty, key=counter_before_dirty.get, reverse=True)]

# only 50 with highest occurency
counter_2k18 = {}
counter_after = {}
counter_before = {}
for entry in sorted_2k18:
    if entry[0] not in stop_words_to_remove:
        counter_2k18[entry[0]] = entry[1]
        if len(counter_2k18) >= 20:
            break

for entry in sorted_after:
    if entry[0] not in stop_words_to_remove:
        counter_after[entry[0]] = entry[1]
        if len(counter_after) >= 20:
            break

for entry in sorted_before:
    if entry[0] not in stop_words_to_remove:
        counter_before[entry[0]] = entry[1]
        if len(counter_before) >= 20:
            break

temp = os.path.join(preselected, "temp", "2018.json")

with open(temp, 'a', encoding='utf-8') as f:
    tweet_str = json.dumps(counter_2k18, default=helpers.converter_for_non_serializable_objetcts)
    f.write(tweet_str + ",")

temp = os.path.join(preselected, "temp", "before.json")
with open(temp, 'a', encoding='utf-8') as f:
    tweet_str = json.dumps(counter_before, default=helpers.converter_for_non_serializable_objetcts)
    f.write(tweet_str + ",")

temp = os.path.join(preselected, "temp", "after.json")
with open(temp, 'a', encoding='utf-8') as f:
    tweet_str = json.dumps(counter_after, default=helpers.converter_for_non_serializable_objetcts)
    f.write(tweet_str + ",")


print("done")

print(overall_pos)
print(overall_neg)

bar_width = 0.35

index = np.arange(len(counter_2k18))

plot1 = plt.bar(index, counter_2k18.values())
plt.ylabel("Worthäufigkeit")
plt.title("Top 20 Worthäufigkeiten in 2018")
plt.xticks(index, counter_2k18.keys(), rotation='vertical')
plt.show()

plot2 = plt.bar(index, counter_after.values())
plt.ylabel("Worthäufigkeit")
plt.title("Top 20 Worthäufigkeiten nach dem 18. September 2015")
plt.xticks(index, counter_after.keys(), rotation='vertical')
plt.show()

