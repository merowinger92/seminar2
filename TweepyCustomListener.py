from tweepy import StreamListener as sListener
import overallconfig as cfg
import json
from datetime import datetime
from TweetModel import TweetModel
from smtplib import SMTP_SSL as SMTP
from email.mime.text import MIMEText
import os
import helpers

def log_something(severity, text):
    text_subtype = 'html'
    destination = [cfg.smtp_creds['receipient']]
    try:
        msg = MIMEText(text, text_subtype)
        msg['Subject'] = severity
        msg['From'] = cfg.smtp_creds['user']  # some SMTP servers will do this automatically, not all

        conn = SMTP(cfg.smtp_creds['server'])
        conn.set_debuglevel(False)
        conn.login(cfg.smtp_creds['user'], cfg.smtp_creds['password'])
        try:
            conn.sendmail(cfg.smtp_creds['password'], destination, msg.as_string())
        finally:
            conn.quit()

    except Exception as exc:
        print("mail failed; " + str(exc))  # give a error message
    return


def extract_tweet_text(status):
    return_text = ""
    # retweet handling
    if "retweeted_status" in status._json:
        if "extended_tweet" in status._json["retweeted_status"]:
            # print("retweet: extended tweet")
            return_text = "RT @"+ status.retweeted_status.author.screen_name + ": " + status._json["retweeted_status"]["extended_tweet"]["full_text"]
        else:
            # print("retweet")
            return_text = "RT @"+ status.retweeted_status.author.screen_name + ": " + status._json["retweeted_status"]["text"]
    elif "extended_tweet" in status._json:
        return_text = status._json["extended_tweet"]["full_text"]
    elif "full_text" in status._json:
        # print("fulltext")
        return_text = status._json["full_text"]
    elif "text" in status._json:
        # print("text")
        return_text = status._json["text"]
    else:
        return_text = status.text
    return return_text



class TweepyCustomListener(sListener):

    buffer = []

    def register_signal_handler(self, signal, frame):
        # this fucntion is used to empty buffer in case programm is interrupted by CTRL + C
        return

    def empty_buffer(self):
        print("Empty Buffer....")
        if len(self.buffer) > 0:
            print("writing down to file...")
            for tweet in self.buffer:
                day = str(tweet.creation_date.day)
                if len(day) < 2:
                    day = '0' + day
                month = str(tweet.creation_date.month)
                if len(month) < 2:
                    month = '0' + month
                file_name = day + month + str(tweet.creation_date.year) + '.json'
                temp_path = os.path.join(*cfg.file_paths['rawdata'])
                path = os.path.join(temp_path, file_name)
                with open(path, 'a', encoding='utf-8') as f:
                    tweet_str = json.dumps(tweet.__dict__, default=helpers.converter_for_non_serializable_objetcts)
                    f.write(tweet_str + ",")
            # reset buffer
            print("remove all entries in buffer since they are saved...")
            self.buffer = []
        return

    def on_status(self, status):
        print("New Tweet arrived...")
        tweet = TweetModel()
        tweet.id = status.id_str
        tweet.username = status.author.screen_name
        tweet.user_id = status.author.id
        tweet.creation_date = status.created_at
        tweet.text = extract_tweet_text(status)
        self.buffer.append(tweet)
        if len(self.buffer) >= cfg.search_buffer['size']:
            self.empty_buffer()
        return


    def on_warning(self, warning):
        log_something("WARNING: YELLOW", warning)

    def on_disconnect(self, notice):
        log_something("ERROR: DISCONNET", notice)

    def on_exception(self, exception):
        log_something("EROROR: UNHANDLED EXCEPTION", exception)

    def on_error(self, status_code):
        log_something(("ERROR: ON_ERROR CALLED", status_code))
        if status_code == 420:
            #sleep for 15 minutes if error code 420 is received
            return True


