import overallconfig as cfg
import json
from TweetModel import TweetModel
from datetime import datetime
from dateutil.parser import parse
from nltk.tokenize import TweetTokenizer
from nltk.stem import PorterStemmer
from nltk.tag.stanford import StanfordPOSTagger as POS_tag
import helpers
import os
from queue import Queue
from threading import Thread


def extraction_worker(i, queue, preproc_path):
    while True:
        print("Thread ", i, ": Waiting for work...")
        file_path = queue.get()
        tweets = read_tweets_from_file(file_path)
        count_total = len(tweets) + 1
        print("Thread ", i, ": Processing File: ", file_path, " with ", count_total, " entries")
        count = 0
        for entry in tweets:
            count = count + 1
            print("Thread ", i, ": Process Tweet ", count, " from ", count_total, " total Tweets in file: ", file_path)
            tweet = entry
            tweet.tokens = tokenize(entry.text)
            tweet.stemmed_tokens = stemming(tweet.tokens)
            tweet.pos_tagged_stems = pos_tag(tweet.stemmed_tokens)
            tweet.pos_tagged_tokens = pos_tag(tweet.tokens)
            tweet.emote_stemms = extract_adv_adj_for_tweet(tweet.pos_tagged_stems)
            tweet.emote_tokens = extract_adv_adj_for_tweet(tweet.pos_tagged_tokens)
            save_preprocessed_data(tweet, preproc_path)
        print("Finished Extraction for file: ", file_path)
        queue.task_done()


def extract_adj_and_adv(raw_path, preproc_path):
    '''we extract only tags which relate to Adjectives and adverbs out of each Tweet
       the result will be saved in preprocessed folder
       procedure:
        1. read cleaned data from raw cleaned data folder
        2. for each TweetModel object do:
            3. Tokenzie with nltk tweetTokenizer
            3. extract adj and adv from text attribute of Tweet
            4. Save as TweetModelPre in preprocessed folder
    '''

    enclosure_queue = Queue()
    '''
    setup Threads
    '''
    for i in range(5):
        worker = Thread(target=extraction_worker, args=(i, enclosure_queue, preproc_path))
        worker.setDaemon(True)
        worker.start()

    for file in os.listdir(raw_path):
        if file.endswith(".json"):
            print("Queuing file: " + file.__str__())
            filepath = os.path.join(raw_path, file)
            enclosure_queue.put(filepath)

    print("Waiting for Extraction to finish...")
    enclosure_queue.join()
    return True


def read_tweets_from_file(file):
    '''returns Array of TweetModel'''
    tweets = []
    JSON_array = helpers.read_JSON_file(file)
    for entry in JSON_array:
        tweet = TweetModel()
        tweet.id = entry['id']
        tweet.username =  entry['username']
        tweet.user_id = entry['user_id']
        tweet.creation_date = parse(entry['creation_date'])
        tweet.text = entry['text']
        tweets.append(tweet)
    return tweets

def tokenize(tweet):
    tknzr = TweetTokenizer();
    return tknzr.tokenize(tweet)

def stemming(words):
    porter = PorterStemmer()
    stemms = []
    for word in words:
        stemm = porter.stem(word)
        stemms.append(stemm)
    return stemms

def pos_tag(stemms):
    model_file = os.path.join(*cfg.pos_tagger['standford_model_path'])
    jar_path = os.path.join(*cfg.pos_tagger['standford_jar_path'])
    stanford_tagger = POS_tag(model_filename=model_file, path_to_jar=jar_path) # , java_options="-Xmx2048m")
    tags = stanford_tagger.tag(stemms)
    return tags

def extract_adv_adj_for_tweet(tweet):
    ''' relevant tags:
        JJ	adjective	'big'
        JJR	adjective, comparative	'bigger'
        JJS	adjective, superlative	'biggest'
        RB	adverb	very, silently,
        RBR	adverb, comparative	better
        RBS	adverb, superlative	best
    '''
    allowed_vals = cfg.pos_tagger['relevant_tags']
    emotes = []
    for pos_tagged in tweet:
        if pos_tagged[1] in allowed_vals:
            emotes.append(pos_tagged[0])
    return emotes

def save_preprocessed_data(data, path):
        temp_path = path
        day = str(data.creation_date.day)
        if len(day) < 2:
            day = '0' + day
        month = str(data.creation_date.month)
        if len(month) < 2:
            month = '0' + month
        file_name = day + month + str(data.creation_date.year) + '.json'
        path = os.path.join(temp_path, file_name)
        with open(path, 'a', encoding='utf-8') as f:
            tweet_str = json.dumps(data.__dict__, default=helpers.converter_for_non_serializable_objetcts)
            f.write(tweet_str + ",")
