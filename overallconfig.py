# Twiter API settings
twitter_api = {
    'consumer_key' : '5UHI8LVJtOLBlFshEcEZ62fnO',
    'consumer_secret' : 'cNH0IMmPo77Vl1eDJFKZmfKrF6BidM3LMxbIlXQRZNocHRCAJQ',
    'access_token' : '755534297801646081-YnB2JqveK7vA1M1HBAsTFFV12LmvnKL',
    'access_secret' : '4whZAtBm76pXNjLBBU6p3HIlWUYNur8eTUzCVAgyIpQOu'
    }

mongoDB_credentials = {
    'server': 'ds155218.mlab.com',
    'port': 55218,
    'user': 'testuser',
    'password': 'unimainz123',
    'database': 'tweets'
}

proxy_creds = {
    'proxyIP' : 'www-proxy.t-online.de',
    'proxyPort' : '80'
}

smtp_creds = {
    'server': 'smtp.gmail.com',
    'port': '465',
    'user': 'maumau7423@gmail.com',
    'password' : '123456aBc',
    'receipient' : 'mathiaskron@gmx.de'
}

search_query = {
    'terms' : ['Volkswagen', 'VW', 'vw', 'volkswagen', 'dieselgate', 'emission fraud', 'vwgate', 'VWgate', 'EA189', 'ea189', '#dieselgate'],
    'operator': 'OR'
}


reference_words = {
    'positive': 'good',
    'negative': 'bad'
}

search_buffer = {
    'size' : 100
}

file_paths = {
    'rawdata' : ['data', 'raw'],
    'rawdata_cleadned' : ['data', 'raw', 'cleaned'],
    'preprocessed' : ['data', 'preprocessed'],
    'preprocessed_dirty' : ['data', 'preprocessed', 'dirty'],
    'preprocessed_words' : ['data', 'preprocessed', 'words'],
    'preselected'   :   ['data', 'preselected'],
    'rawdata_crawled' : ['data', 'raw', 'crawled'],
    'words' :   ['data', 'words'],
    'preprocessed_counted_words'    : ['data', 'preprocessed', 'counted_words'],
    'preprocessed_sentiment_vectors'    : ['data', 'preprocessed', 'weighted_vectors'],
    'clustering'    :   ['data', 'clustering'],
    'clustered'     :   ['data', 'clustered']
}

past_search_periods = {
    'period1' : {
        'start' : '2015-07-18',
        'end': '2015-07-25'
    },
    'period2' : {
        'start' : '2015-07-25',
        'end' : '2015-08-01'
    },
    'period3' : {
        'start' : '2015-08-01',
        'end' : '2015-08-08'
    },
    'period4' : {
        'start' : '2015-08-08',
        'end' : '2015-08-15'
    },
    'period5' : {
        'start' : '2015-08-15',
        'end' : '2015-08-22'
    },
    'period6' : {
        'start' : '2015-08-22',
        'end' : '2015-08-29'
    },
    'period7' : {
        'start' : '2015-08-29',
        'end' : '2015-09-05'
    },
    'period8' : {
        'start' : '2015-09-05',
        'end' :  '2015-09-12'
    },
    'period9' : {
        'start' : '2015-09-12',
        'end' : '2015-09-19'
    },
    'period10' : {
        'start' : '2015-09-19',
        'end' : '2015-09-26'
    },
    'period11' : {
        'start' : '2015-09-26',
        'end' : '2015-10-03'
    },
    'period12' : {
        'start' : '2015-10-03',
        'end' : '2015-10-10'
    },
    'period13' : {
        'start' : '2015-10-10',
        'end' : '2015-10-17'
    },
    'period14' : {
        'start' : '2015-10-17',
        'end' : '2015-10-24'
    },
    'period15' : {
        'start' : '2015-10-24',
        'end' : '2015-10-31'
    },
    'period16' : {
        'start' : '2015-10-31',
        'end' : '2015-11-07'
    },
    'period17' : {
        'start' : '2015-11-07',
        'end' : '2015-11-14'
    },
    'period18' : {
        'start' : '2015-11-14',
        'end'   : '2015-11-21'
    }
}

pos_tagger = {
    'relevant_tags' : ['JJ', 'JJR', 'JJS', 'RB', 'RBR', 'RBS'],
    'standford_model_path'  :   ['libs', 'stanford-postagger', 'models', 'english-left3words-distsim.tagger'],
    'standford_jar_path'  :   ['libs', 'stanford-postagger', 'stanford-postagger.jar']
}


clustering_times = 20

offline_mode = True





