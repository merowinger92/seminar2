import Preprocessor
import os
import helpers
import Converter
import Weighting


def preprocessing(file_paths):
    ''' do something '''
    print("Starting Preprocessing Procedure")
    raw_directory = os.path.join(*file_paths['rawdata'])
    preproc_directory = os.path.join(*file_paths['preprocessed_dirty'])
    Preprocessor.extract_adj_and_adv(raw_directory, preproc_directory)
    ''' TODO: Preprocessing for preselected tweets'''

def convert_documents_to_vectors(file_paths):
    ''' do something '''
    print("convert documents to vectors")
    preproc_directory = os.path.join(*file_paths['preprocessed_dirty'])
    words_path = os.path.join(*file_paths['preprocessed_words'])
    word_vectors = os.path.join(*file_paths['preprocessed_counted_words'])
    Converter.convert_tweets_to_vectors(preproc_directory, word_vectors, words_path)
    print("Finished Convertering Tweets to Vectors...")


def weighting_vectors(args):
    ''' do something '''
    print("weighting vectors")


def clustering(args):
    ''' do something '''
    print("clustering")


def voting(args):
    ''' do something'''
    print("voting")


def run(file_paths_dict,args):
    helpers.setup() # setup filepaths
    preprocessing(file_paths_dict)
    convert_documents_to_vectors(file_paths_dict)
    weighting_vectors(args)
    clustering(args)
    voting(args)